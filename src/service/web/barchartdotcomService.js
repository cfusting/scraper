var http = require('http');
var querystring = require('querystring');

/**
 * Get the HTML content of a chart page.
 * Try and make this look like a real request...
 * @param symbol the price symbol
 * @param startDate Format mm/dd/yyyy
 * @param endDate Format mm/dd/yyyy
 * @param done callback
 */
exports.getPriceHtml = function(symbol, startDate, endDate, done) {
    var barChartParams = {
        sym: symbol.toUpperCase(),
        style: 'technical',
        template: '',
        p: 'DO',
        d: 'M',
        sd: startDate,
        ed: endDate,
        size: 'M',
        log: 0,
        t: 'BAR',
        v: 2,
        g: 1,
        evnt: 1,
        late: 1,
        o1: '',
        o2: '',
        o3: '',
        sh: 100,
        indicators: '',
        addindicator: '',
        submitted: 1,
        fpage: '',
        txtDate: ''
    };
    var req = http.request({
        hostname: 'www.barchart.com',
        path: '/chart.php?' + querystring.stringify(barChartParams)
    }, function(res) {
        if (res.statusCode != 200) {
            console.error('Server responded with code: ' + res.statusCode);
            return done(new Error('Could not retrieve data from server.'), '', symbol);
        }
        var data = '';
        res.setEncoding('utf8');
        res.on('data', function(chunk) {
            data += chunk;
        });
        res.on('end', function() {
            return done('', data.toString(), symbol);
        });
    });
    req.on('error', function(err) {
        console.error('Problem with request: ', err);
        return done(err, '');
    });
    req.end();
}
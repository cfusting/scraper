var pg = require('pg');

var futures = require('../../utils/futuresContracts/futuresContractSpecifications.js');
var debug = require('../../../config/debug.js');

/**
 * Rollback a failed transaction.
 * @param client the client
 * @param done function
 */
var rollback = function(client, done) {
    client.query('ROLLBACK', function(err) {
        return done(err);
    });
};

/**
 * Create a table to hold the price data.
 * @param connectConfig The connection configuration
 * @param symbol the table name
 * @param complete callback
 */
exports.createPriceTable = function(connectConfig, symbol, complete) {
    var errorMsg = 'Error creating price table for symbol ' + symbol;
    pg.connect(connectConfig, function(err, client, done) {
        if (err) {
            console.error(errorMsg, err);
            return complete(err);
        }
        client.query('BEGIN', function(err) {
            if (err) {
                console.error(errorMsg, err);
                rollback(client, done);
                return complete(err);
            }
            process.nextTick(function() {
            });
            client.query(
                'CREATE TABLE IF NOT EXISTS '
                    + symbol
                    + '(id serial PRIMARY KEY,'
                    + 'timestamp timestamp,'
                    + 'open numeric,'
                    + 'high numeric,'
                    + 'low numeric,'
                    + 'close numeric,'
                    + 'volume numeric,'
                    + 'interest numeric);'
                    + 'CREATE UNIQUE INDEX ON ' + symbol + ' (timestamp);'
                    + 'COMMIT;',
                function(err) {
                    if (err) {
                        console.error(errorMsg, err);
                        rollback(client, done);
                        return complete(err);
                    }
                    client.query('COMMIT', done);
                    complete();
                });
        });
    });
};

/**
 * Save price data into a postgres database.
 * @param err callback
 * @param connectConfig The connection parameters
 * @param symbol the symbol in which to append the data
 * @param price the price data object
 * @param complete callback
 */
exports.savePriceData = function(connectConfig, symbol, price, complete) {
    var errorMsg = 'Error saving price data for symbol ' + symbol;
    pg.connect(connectConfig, function(err, client, done) {
        if (err) {
            console.error(errorMsg, err);
            return complete(err);
        }
        var stream = client.copyFrom('COPY '
            + symbol
            + ' (timestamp, open, high, low, close, volume, interest) FROM STDIN WITH DELIMITER \'|\' NULL \'\'');
        stream.on('close', function() {
            console.log('Data load complete for symbol: ' + symbol);
            return complete();
        });
        stream.on('error', function(err) {
            console.error(errorMsg, err);
            return complete(err);
        });
        for (var i in price) {
            var r = price[i];
            stream.write(i + '|' + r[0] + '|' + r[1] + '|' + r[2] + '|' + r[3] + '|' + r[4] + '|' + r[5] + '\n');
        }
        stream.end();
        done();
        complete();
    });
};

/**
 * Creates a view in the database representing the "nearest month" time series data for a given
 * contract symbol.  Assumes the necessary data has been loaded using {@link savePriceData} for
 * the time span in question.
 *
 * @param connectConfig
 * @param symbol
 * @param startYear
 * @param endYear
 * @param complete
 */
exports.createTimeSeries = function(connectConfig, symbol, startYear, endYear, complete) {
    var errorMsg = 'Error creating time series view for contract ' + symbol;
    pg.connect(connectConfig, function(err, client, done) {
        if (err) {
           console.error(errorMsg, err);
           complete(err);
        }
        var statement = getCreateViewText(symbol + '_time_series') + futures.getTimeSeriesDll(startYear, endYear, symbol);
        if (debug.debug) return console.log(statement);
        client.query(statement, function(err) {
            if (err) {
              console.error(errorMsg, err)
              complete(err);
            }
            complete('');
       });
       done();
    });
};

/**
 * Get the text for creating a view.
 * @param table
 * @returns {string}
 */
var getCreateViewText = function(table) {
    return 'CREATE OR REPLACE VIEW ' + table + ' AS ';
};



exports.contracts =
    {
        symbol: 'zcn13',
        startDate: '12/15/2011',
        endDate: '12/15/2013'
    };

var MILL = 2000;

var MONTH_SYMBOLS = {
    1: 'f',
    2: 'g',
    3: 'h',
    4: 'j',
    5: 'k',
    6: 'm',
    7: 'n',
    8: 'q',
    9: 'u',
    10: 'v',
    11: 'x',
    12: 'z'
};

var MONTHS = 'months';
var END_DAY = 'endDay';
var HISTORY = 'history';

var CONTRACT_SYMBOLS = {
    'zc': {
        'months': [3,5,7,9,12],
        'endDay': 15,
        'history': 2
    },
    'zs': {
        'months': [1,3,5,7,8,9],
        'endDay': 15,
        'history': 2
    },
    'zw': {
        'months': [3,5,7,9,12],
        'endDay': 15,
        'history': 2
    },
    'dx': {
        'months': [3,6,9,12],
        'endDay': 15,
        'history': 1
    }
};

/**
 * Get contracts for a given time span and symbol
 * @param firstYear
 * @param lastYear
 * @param symbol
 * @returns [{}]
 */
exports.getContractsForSymbol = function(firstYear, lastYear, symbol) {
    if (CONTRACT_SYMBOLS[symbol] == undefined) {
        console.error('Symbol not recognized: ' + symbol);
        return [];
    }
    var months = CONTRACT_SYMBOLS[symbol][MONTHS];
    var endDay = CONTRACT_SYMBOLS[symbol][END_DAY];
    var history = CONTRACT_SYMBOLS[symbol][HISTORY];
    var contracts = [];
    var c = 0;
    for (var i = lastYear - firstYear; i >= 0; i--) {
        for (var a = 0; a < months.length; a++) {
            contracts[c] =  {
                symbol: symbol + MONTH_SYMBOLS[months[a]] + getLiteralNumeral((firstYear + i) - MILL),
                startDate: getLiteralNumeral(months[a]) + '/' + endDay + '/' + ((firstYear + i) - history),
                endDate: getLiteralNumeral(months[a]) + '/' + endDay + '/' + (firstYear + i)
            };
            c++;
        }
    }
    return contracts;
};

/**
 * Get a DLL representing a view of time series future data.
 * @param startYear
 * @param endYear
 * @param symbol
 * @returns {*}
 */
exports.getTimeSeriesDll = function(startYear, endYear, symbol) {
    if (CONTRACT_SYMBOLS[symbol] == undefined) {
        console.error('Symbol not recognized: ' + symbol);
        return '';
    }
    var months = CONTRACT_SYMBOLS[symbol][MONTHS];
    var endDay = CONTRACT_SYMBOLS[symbol][END_DAY];
    var startMonths = getStartMonths(months);
    var years = getYearsArray(startYear, endYear);
    var dll = '';
    for (var i = 0; i < years.length; i++) {
        for (var a = 0; a < startMonths.length; a++) {
            var firstYear = a == 0 ? years[i] - 1 : years[i];
            dll += 'SELECT * FROM ' + symbol + MONTH_SYMBOLS[months[a]] + getLiteralNumeral(years[i] - MILL)
                + ' WHERE timestamp > \'' + firstYear + '-' + getLiteralNumeral(startMonths[a]) + '-' + endDay
                + '\' AND timestamp <= \'' + years[i] + '-' + getLiteralNumeral(months[a]) + '-' + endDay + '\' ';
            if (!((i == years.length - 1) && (a == startMonths.length - 1))) {
                dll += 'UNION ';
            }
        }
    }
    return dll;
};

/**
 * Get the literal numeral value for timestamps.
 * @param numeral
 * @returns {*}
 */
var getLiteralNumeral = function(numeral) {
    if (numeral < 10) {
        return '0' + numeral;
    }
    return numeral;
};

/**
 * Get an array of months representing the start date.
 * @param months
 * @returns {Array}
 */
var getStartMonths = function(months) {
    var endMonths = [months[months.length - 1]] ;
    for(var i = 0; i < months.length - 1; i++) {
        endMonths[i + 1] = months[i];
    }
    return endMonths;
};

/**
 * Get an array of years from start to finish.
 * @param startYear
 * @param endYear
 * @returns {Array}
 */
var getYearsArray = function(startYear, endYear) {
    var span = endYear - startYear;
    var years = [];
    for (var i = 0; i <= span; i++) {
        years[i] = startYear + i;
    }
    return years;
};

var cheerio = require('cheerio');

var chart = require('../../domain/chart');

/**
 * Scrape data from a page given a DOM selector and a pattern.
 * @param html the body of the page
 * @param selector the selector
 * @param pattern the pattern
 */
exports.scrapePriceData = function(html) {
    var price = {};
    $ = cheerio.load(html);
    $('area','#chartdiv').each(function(i) {
        var data = $(this).attr('onmousemove');
        if (data.match('.*Volume')) {
            var matched = data.match('^.*\\[.*\\.\\s(.+?)\\]\', \'.*?\', \'(.+)\'\\)');
            var date = matched[1];
            if (price[date] == undefined) {
                price[date] = {};
            }
            if (price[date][chart.VOLUME] == undefined) {
                price[date][chart.VOLUME] = [];
            }
            price[date][chart.VOLUME][price[date][chart.VOLUME].length] = matched[2];
        }
        else if (data.match('.*Interest')) {
            var matched = data.match('^.*\\[.*\\.\\s(.+?)\\]\', \'.*?\', \'(.+)\'\\)');
            var date = matched[1];
            if (price[date] == undefined) {
                price[date] = {};
            }
            if (price[date][chart.INTEREST] == undefined) {
                price[date][chart.INTEREST] = [];
            }
            price[date][chart.INTEREST][price[date][chart.INTEREST].length] = matched[2]
        } else {
            var matched = data.match('^.*\\[.*\\.\\s(.+?)\\]\', \'.*?\', \'(.+)\', \'(.+)\', \'(.+)\', \'(.+)\'\\)');
            var date = matched[1];
            if (price[date] == undefined) {
                price[date] = {};
            }
            if (price[date][chart.OHLC] == undefined) {
                price[date][chart.OHLC] = [];
            }
            price[date][chart.OHLC][price[date][chart.OHLC].length] = [matched[2], matched[3], matched[4], matched[5]];
        }
    });
    return cleanPriceData(price);
};

/**
 * Clean the price data object.
 * @param price
 * @returns {{}}
 */
var cleanPriceData = function(price) {
    var cleanPrice = {};
    for (var i in price) {
        cleanPrice[i] = cleanPriceRow(price[i]);
    }
    return cleanPrice;
};

/**
 * Clean up the scraped data so it is suitable for the database.
 * @param priceRow a row of data.
 * @returns {Array}
 */
var cleanPriceRow = function(priceRow) {
        var row = [];
        if (priceRow[chart.OHLC] == undefined) {
            for (var i = 0; i < 4; i++) {
                row[i] = '';
            }
        } else if (priceRow[chart.OHLC].length > 1) {
            console.log("Error: Multiple values for OHLC: " + priceRow[chart.OHLC]);
            process.exit();
        } else {
            for (var i = 0; i < 4; i++) {
                row[i] = priceRow[chart.OHLC][0][i];
            }
        }
        if (priceRow[chart.VOLUME] == undefined) {
            row[4] = '';
        } else {
            row[4] = cleanVolumeData(priceRow[chart.VOLUME]);
        }
        if (priceRow[chart.INTEREST] == undefined) {
            row[5] = '';
        } else if (priceRow[chart.INTEREST].length > 1) {
            console.error("Error: Multiple values for Interest: " + priceRow[chart.INTEREST]);
            process.exit();
        } else {
            row[5] = priceRow[chart.INTEREST][0];
        }
    return row;
};

/**
 * Cleans duplicate volume data.
 * If all volume data is 0, return 0.  If one member of the data is greater than 0, return that member.
 * If two or more members of the data are greater than 0 return null as the value is not reliable.
 * @param vol an array of volume data
 * @returns the value or null if the data is bad
 */
var cleanVolumeData = function(vol) {
    if (vol.length == 1) {
        return vol[0];
    } else {
        var trueVal = 0;
        for (var i = 0; i < vol.length; i++) {
            if (vol[i] > 0) {
                if (trueVal == 0) {
                    trueVal = vol[i];
                } else {
                    return null;
                }
            }
        }
        return trueVal;
    }
};
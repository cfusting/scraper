var fs = require('fs')

var futureContractService = require('./service/futuresContracts/futureContractService');
var localhost = require('./../config/postgresLocalhost');
var scraper= require('./utils/scraper/barchartdotcomScraper');
var barchart = require('./service/web/barchartdotcomService');
var futures = require('./utils/futuresContracts/futuresContractSpecifications');
var debug = require('./../config/debug.js');

/**
 * Load all available price data for a given futures contract symbol.
 * @param connectConfig
 * @param contract
 * @param done
 */
var loadPriceDataForContract = function (connectConfig, contract, done) {
    var errMsg = 'Failed to process symbol: ';
    console.log('Processing symbol: ' + contract.symbol);
    barchart.getPriceHtml(contract.symbol, contract.startDate, contract.endDate, function(err, html) {
        if (err) {
            console.error(errMsg + contract.symbol);
            done(err);
            return;
        }
        var priceData = scraper.scrapePriceData(html);
        if (debug.debug) return console.log(priceData);
        futureContractService.createPriceTable(connectConfig, contract.symbol, function(err) {
            if (err) {
                console.error(errMsg + contract.symbol);
                done(err);
                return;
            }
            futureContractService.savePriceData(connectConfig, contract.symbol, priceData, function(err) {
                if (err) {
                    console.error(errMsg + contract.symbol);
                    done(err);
                    return;
                }
                done('');
            });
        });
    });
};

/**
 * Run.
 */
var run = function() {
    var startYear = 2008;
    var endYear = 2013;
    var symbol = 'dx';

//    var contracts = futures.getContractsForSymbol(startYear, endYear, symbol);
//    for (var i = 0; i < contracts.length; i++) {
//        var contract = contracts[i];
//        loadPriceDataForContract(localhost.connectConfig, contract, function(err) {
//            if (err) {
//                console.error('Failed to load future contract data', err);
//            }
//        });
//        // Process one contract and exit.
//        if (debug.debug) break;
//    }

    futureContractService.createTimeSeries(localhost.connectConfig, symbol, startYear, endYear, function(err) {
        if (err) return;
        console.log('Created time series for symbol: ' + symbol);
    });

};

run();
